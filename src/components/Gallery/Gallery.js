import React, { useEffect, useState } from 'react';
import Slider from "react-slick";
import axios from 'axios';
import './gallery.css';
import SimpleDialog from './dialog';
import Typography from '@material-ui/core/Typography';

function Gallery(props) {
    const [bid, setBid] = useState([]);
    const [open, setOpen] = useState(false);
    const [selectedValue, setSelectedValue] = useState([]);
    const [sendValue, setValue] = useState({});


    useEffect(() => {
        axios.get(`http://zenbidsales.eastus.azurecontainer.io/livesales`)
            .then(res => {
                const bidDetails = res.data;
                setBid(bidDetails);
            })
    }, []);
    var settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 2
    }


    const handleClickOpen = (e) => {
        setOpen(true);
        setValue(e);
    };

    const handleClose = (value) => {
        setOpen(false);
        setSelectedValue(value);
    };

    return (
        <React.Fragment>
            <Typography component="h2" variant="h6" color="primary" className="galleryheader" gutterBottom>
                Live Sale Content            
            </Typography>
            <Slider {...settings}>
                {bid.map(bidResponse => <div key={bidResponse.imageSrc}><h1>
                    <img onClick={(e) => { handleClickOpen(bidResponse) }} src={bidResponse.imageSrc} alt="" /></h1></div>)}

            </Slider>
            <div>

                <SimpleDialog toggleCamera={props.toggleCamera} selectedValue={selectedValue} sendValue={sendValue} open={open} onClose={handleClose} />
            </div>
        </React.Fragment>

    );

}

export default Gallery
