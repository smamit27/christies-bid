import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import { blue } from '@material-ui/core/colors';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import MuiDialogContent from '@material-ui/core/DialogContent';
import { withStyles } from '@material-ui/core/styles';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CameraIcon from '@material-ui/icons/PhotoCamera';

const useStyles = makeStyles({
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  }, inline: {
    display: 'inline',
  },
  cardMedia: {
    paddingTop: '86.25%', // 16:9
  },
});

function SimpleDialog(props) {
  const classes = useStyles();
  const { onClose, selectedValue, open } = props;
  const handleClose = () => {
    onClose(selectedValue);
  };
  const handleMultipleProps =() =>{
    props.toggleCamera();
    props.onClose();
  }
  const DialogContent = withStyles((theme) => ({
    root: {
      padding: theme.spacing(2),
    },
  }))(MuiDialogContent);
 

  return (
    <Dialog onClose={handleClose} aria-labelledby="simple-dialog-title" open={open}>
      <DialogTitle id="customized-dialog-title" onClose={handleClose}>
      {props.sendValue.title}
    </DialogTitle>
        <DialogContent dividers>

      <Container className={classes.cardGrid} maxWidth="md">
          <Grid container spacing={4}>
              <Grid item  xs={12} sm={12} md={12}>
                <Card className={classes.card}>
                  <CardMedia
                    className={classes.cardMedia}
                    image={props.sendValue.imageSrc}
                    title="Image title"
                  />
                  <CardContent className={classes.cardContent}>
                  <Typography>
                    Sale Number: {props.sendValue.saleNumber}
                    </Typography>
                    <Typography>
                     Start Date:  {props.sendValue.startDate}
                    </Typography>
                    <Typography>
                     End Date:  {props.sendValue.endDate}
                    </Typography>
                    <Typography>
                    Title: {props.sendValue.title}
                    </Typography>
                  </CardContent>
              </Card>
              </Grid>
          </Grid>
        </Container>
   </DialogContent>
   <DialogActions>
   <IconButton color="inherit" className={classes.posit} onClick={handleMultipleProps} >
            <CameraIcon  className={classes.icon} />
  </IconButton>
          <Button  color="primary">
            Save changes
          </Button>
        </DialogActions>
    </Dialog>
  );
}

export default SimpleDialog;



