import React from 'react';
import logo from '../data/live.gif'; 
function Live()  {
    const live = {
        width:'100px'
    }
    return (
        <React.Fragment>
            <span>
                <img src={logo} alt="Logo" style={live}/>
            </span>
        </React.Fragment>
    )
}
export default Live;