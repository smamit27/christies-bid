import React, { useState, useEffect } from 'react';
import './App.css';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Gallery from './components/Gallery/Gallery';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import CameraIcon from '@material-ui/icons/PhotoCamera';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import AppBar from '@material-ui/core/AppBar';
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official';
import Webcam from "react-webcam";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import axios from 'axios';
import Live from './components/live';



const useStyles = makeStyles((theme) => ({
  icon: {
    marginRight: theme.spacing(2)
  },
  inline:{
    display: 'block'
  },
  posit: {
    position: 'absolute',
    right: '0px'
  },
   marginTop: {
    marginTop: '20px'
  },
  
}));
function App() {
  const classes = useStyles();
  const [camera, setCamera] = useState(false);
  const [bidCharData, setBidChart] = useState([])
  const [bid,setBid] = useState([]);
  useEffect(() => {
    axios.get(`http://zenbidsales.eastus.azurecontainer.io/livesales`)
      .then(res => {
       const filterChart = res.data.filter(chart => chart.bidAmount >= 500);
       const slicePlotChart = filterChart.slice(0,2);
       const bidDetails = res.data.map(x => Number(x.bidAmount));
        setBidChart(bidDetails);
        setBid(slicePlotChart);

      })
  }, []);
  const toggleCamera = () =>{
    setCamera(!camera);
  }
  const options = {
    title: {
      text: ' Bidding Amount Chart'
    },
    series: [{
      data: bidCharData
    }]
  }
  return (
    <div className="App">
      <CssBaseline />
      <AppBar position="relative">
        <Toolbar>
          <Typography variant="h6" color="inherit" noWrap>
            Christies Live  
          </Typography>
          <Live />
          <IconButton color="inherit" className={classes.posit} onClick={() =>toggleCamera()}>
            <CameraIcon className={classes.icon} />
          </IconButton>
        </Toolbar>
      </AppBar>
      <Container maxWidth="lg" className={classes.marginTop}>
        <Grid container>
            <Grid item xs={12} md={6} lg={6} className="MuiPaper-elevation1 margin-right">
              <HighchartsReact highcharts={Highcharts} options={options} />
        </Grid>

          {camera ? (
            <Grid item xs={12} md={6} lg={6} className="MuiPaper-elevation1">
              <Webcam />
            </Grid>) : null}
          <Grid item xs={12} md={6} lg={6} className="MuiPaper-elevation1">
            <Typography component="h2" variant="h6" color="primary" gutterBottom>
              Recent Bid Price   
            </Typography>
            <Typography component="p" variant="h4">
              $3,024.00
            </Typography>
            <Typography color="textSecondary" >
              on 15 March, 2019
            </Typography>
            { bid.map((chart,index) => (
            <List className={classes.root} key={index}>
            <ListItem alignItems="flex-start">
              <ListItemAvatar>
                <Avatar alt="Remy Sharp" src={chart.imageSrc} />
              </ListItemAvatar>
              <ListItemText
                primary={chart.title}
                secondary={
                  <React.Fragment>
                    <Typography
                      component="span"
                      variant="body2"
                      className={classes.inline}
                      color="textPrimary">
                   Sale Number: {chart.saleNumber}
                                
                   </Typography>
                   <Typography
                      component="span"
                      variant="body2"
                      className={classes.inline}
                      color="textPrimary">
                   Bid Amount: {chart.bidAmount}
                                
                   </Typography>
                   <Typography
                      component="span"
                      variant="body2"
                      className={classes.inline}
                      color="textPrimary">
                      Start Date : {chart.startDate}
                                
                   </Typography>
                   <Typography
                      component="span"
                      variant="body2"
                      className={classes.inline}
                      color="textPrimary">
                      End Date :  ${chart.endDate}
                                
                   </Typography>             
                   </React.Fragment>
                }
              />
            </ListItem>
          </List>)
            )}


          </Grid>
        </Grid>
      </Container>
      <Gallery toggleCamera={toggleCamera}/>

    </div>);
}


export default App;
